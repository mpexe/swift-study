//
//  WeatherLoader.swift
//  Lesson12
//
//  Created by Alexey on 04.09.2020.
//  Copyright © 2020 Alexey. All rights reserved.
//

import Foundation
import Alamofire
import RealmSwift

protocol WeatherLoaderDelegate {
    func loaded(weather: [String:String])
}

class WeatherLoader {
    var delegate: WeatherLoaderDelegate?
    
    func loadWeather(){
        let url = URL(string: "http://api.openweathermap.org/data/2.5/weather?q=Moscow,ru&appid=0c773064f088cdb4529f32255a4cda43")!
        
        var request = URLRequest(url: url)
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            if let data = data,
                let json = try? JSONSerialization.jsonObject(with: data, options: .allowFragments),
                let jsonDict = json as? NSDictionary {
                
                var weather: [String:String] = [:]
                let main = jsonDict["main"]! as? NSDictionary
                let temp = Int((main!["temp"]! as! NSNumber)) - 273
                let weatherKey = jsonDict["weather"] as! NSArray
                let weatherData = weatherKey[0] as! NSDictionary
                let weatherType = weatherData["main"] as! String
                let icon = weatherData["icon"] as! String
                let humidity = Int(main!["humidity"] as! NSNumber)
                let pressure = Int((main!["pressure"] as! Double) / 133.3224 * 100)
                let visability = Int(jsonDict["visibility"]! as! NSNumber) / 1000
                
                weather["temp"] = String(temp) + " \u{2103}"
                weather["weatherType"] = weatherType
                weather["humidity"] = String(humidity) + " %"
                weather["pressure"] = String(pressure) + " мм.рт.ст"
                weather["visibility"] = String(visability) + " км"
                weather["icon"] = icon
                
                
                DispatchQueue.main.async {
                    self.delegate?.loaded(weather: weather)
                }
                
            }
        }
        task.resume()
    }
    
    func loadWeatherDaily(completion: @escaping ([Forecast]) -> Void){
        AF.request("https://api.openweathermap.org/data/2.5/onecall?lat=55.751244&lon=37.618423&exclude=minutely,hourly&units=metric&appid=0c773064f088cdb4529f32255a4cda43").responseJSON
            { response in
            if let objects = response.value,
                let jsonDict = objects as? NSDictionary {
                let dailyData = jsonDict["daily"] as! Array<Any>
                var data = [Forecast]()
                for item in dailyData {
                    let itemDict = item as! NSDictionary
                    let date = NSDate(timeIntervalSince1970: TimeInterval(itemDict["dt"] as! Int))
                    let dateFormatter = DateFormatter()
                    dateFormatter.locale = Locale(identifier: "ru_RU")
                    dateFormatter.dateFormat = "dd MMMM"
                    let stringDate: String = dateFormatter.string(from: date as Date)
                    let feelsLike = itemDict["feels_like"] as! NSDictionary
                    let temperature = Int(feelsLike["day"]! as! Double)
                    let weatherData = itemDict["weather"]! as! [[String:Any]]
                    let main = weatherData[0]["main"]! as! String
                    let icon = weatherData[0]["icon"]! as! String
                    let weatherObject = Forecast(date: stringDate, icon: icon, temperature: String(temperature), type: main)
                    data.append(weatherObject)
                    
                     
                }
                DispatchQueue.main.async {
                    completion(data)
                    
                }
                
                }
                    
                
            
                
                
            
            
            }
    }
    
    func saveWeatherData(withData data: [Forecast]) -> [ForecastObject] {
        let realm = try! Realm()
        
        var realmData = [ForecastObject]()
        
        for cast in data {
            realm.beginWrite()
            let newItem = ForecastObject()
            newItem.date = cast.date!
            newItem.icon = cast.icon
            newItem.temperature = cast.temperature!
            newItem.type = cast.type!
            realm.add(newItem)
            try! realm.commitWrite()
            realmData.append(newItem)
        }
        
        return realmData
        
        
    }
    
}
