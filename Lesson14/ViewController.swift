//
//  ViewController.swift
//  Lesson14
//
//  Created by Alexey on 18.09.2020.
//  Copyright © 2020 Alexey. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var surnameTextField: UITextField!
    
    @IBAction func addPersonButton(_ sender: Any) {
        
        PersonData.shared.name = nameTextField.text
        PersonData.shared.surname = surnameTextField.text
        
        print("\(PersonData.shared.name) \(PersonData.shared.surname)")
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (PersonData.shared.name != nil) && (PersonData.shared.surname != nil) {
            nameTextField.text = PersonData.shared.name!
            surnameTextField.text = PersonData.shared.surname!
        }
        
    }


}

