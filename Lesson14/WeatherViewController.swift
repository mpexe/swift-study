//
//  WeatherViewController.swift
//  Lesson12
//
//  Created by Alexey on 09.09.2020.
//  Copyright © 2020 Alexey. All rights reserved.
//

import UIKit
import RealmSwift

class WeatherViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var weatherTableView: UITableView!
    var weatherData = [Forecast]()
    
    private let realm = try! Realm()
    private var realmWeatherData = [ForecastObject]()
    
    lazy var weatherDataDB: Results<ForecastObject> = { self.realm.objects(ForecastObject.self) }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        realmWeatherData = realm.objects(ForecastObject.self).map({ $0 })
        print("Load from Realm")
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(4), execute: {
            WeatherLoader().loadWeatherDaily { data in
                self.weatherData = data
                self.realmWeatherData = WeatherLoader().saveWeatherData(withData: data)
                self.weatherTableView.reloadData()
                
                print("Now from API")
            }
        })
        
        
        
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return realmWeatherData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DayCell") as! WeatherCell
        cell.dateLabel.text = realmWeatherData[indexPath.row].date
        cell.tempLabel.text = "\(realmWeatherData[indexPath.row].temperature) \u{2103}"
        cell.typeLabel.text = realmWeatherData[indexPath.row].type
        cell.weatherIcon.load(url: URL(string: realmWeatherData[indexPath.row].icon)!)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100.0
    }

}

extension UIImageView {
    func load(url: URL) {
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self?.image = image
                    }
                }
            }
        }
    }
}

