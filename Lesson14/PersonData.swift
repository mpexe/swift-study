import Foundation

class PersonData {
    
    static let shared = PersonData()
    
    private let kNameKey = "PersonData.kNameKey"
    
    var name: String? {
        set { UserDefaults.standard.set(newValue, forKey: kNameKey) }
        get { return UserDefaults.standard.string(forKey: kNameKey) }
    }
    
    private let kSurnameKey = "PersonData.kSurnameKey"
    
    var surname: String? {
        set { UserDefaults.standard.set(newValue, forKey: kSurnameKey) }
        get { return UserDefaults.standard.string(forKey: kSurnameKey) }
    }
    
}
