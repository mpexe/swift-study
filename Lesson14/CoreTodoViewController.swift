//
//  CoreTodoViewController.swift
//  Lesson14
//
//  Created by Alexey on 28.09.2020.
//  Copyright © 2020 Alexey. All rights reserved.
//

import UIKit
import CoreData

class CoreTodoViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    var tasks: [Tasks] = []
    
    @IBAction func deleteTasks(_ sender: Any) {
        tableView.isEditing = !tableView.isEditing
        
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        let context = appDelegate.persistentContainer.viewContext
//
//        let fetchRequest: NSFetchRequest<Tasks> = Tasks.fetchRequest()
//
//        if let tasks = try? context.fetch(fetchRequest) {
//            for task in tasks {
//                context.delete(task)
//            }
//        }
//
//        do {
//            try context.save()
//        } catch let error as NSError {
//            print(error.localizedDescription)
//        }
//
//        tableView.reloadData()
        
    }
    
    func saveTask(withTitle title: String) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        guard let entity = NSEntityDescription.entity(forEntityName: "Tasks", in: context) else { return }
        
        let taskObject = Tasks(entity: entity, insertInto: context)
        taskObject.title = title
        
        do {
            try context.save()
            tasks.append(taskObject)
        } catch let error as NSError {
            
        }
    }
    
    func removeTasks(withTitle title: String) {
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let fetchRequest: NSFetchRequest<Tasks> = Tasks.fetchRequest()
        
        do {
            tasks = try context.fetch(fetchRequest)
        } catch let error as NSError {
            print(error.localizedDescription)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self

    }
    
    @IBAction func addTaskButton(_ sender: Any) {
        let alertController = UIAlertController(title: "New task", message: "Type some text", preferredStyle: .alert)
        
        let saveTask = UIAlertAction(title: "Save", style: .default) {action in
            let tf = alertController.textFields?.first
            if let newTask = tf?.text {
                if newTask == "" {}
                else {
                    self.saveTask(withTitle: newTask)
                    self.tableView.reloadData()
                }
                
            }
        }
        
        alertController.addTextField { _ in }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default) { _ in }
        
        alertController.addAction(saveTask)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
    

    // - MARK: Table View
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tasks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "coreCell") as! CoreTableViewCell
        let task = tasks[indexPath.row]
        cell.textLabel?.text = task.title
        return cell
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .delete
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let task = tasks[indexPath.row]
            tasks.remove(at: indexPath.row)
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let context = appDelegate.persistentContainer.viewContext
    
            let fetchRequest: NSFetchRequest<Tasks> = Tasks.fetchRequest()
    
            do {
                context.delete(task)
                try context.save()
            } catch let error as NSError {
                print(error.localizedDescription)
            }
    
            tableView.reloadData()
        }
    }
}
