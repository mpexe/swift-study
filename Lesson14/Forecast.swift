//
//  Forecast.swift
//  Lesson12
//
//  Created by Alexey on 09.09.2020.
//  Copyright © 2020 Alexey. All rights reserved.
//

import Foundation
import RealmSwift

class Forecast {
    var date: String?
    var icon = ""
    var temperature: String?
    var type: String?
    
    init(date: String, icon: String, temperature: String, type: String) {
        self.date = date
        self.icon = "http://openweathermap.org/img/wn/\(icon)@2x.png"
        self.temperature = temperature
        self.type = type
    }
}

class ForecastObject: Object {
    @objc dynamic var date: String = ""
    @objc dynamic var icon = ""
    @objc dynamic var temperature: String = ""
    @objc dynamic var type: String = ""
    
}
